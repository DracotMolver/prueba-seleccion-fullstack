const CharactersExternalApi = require('../../service/charactersExternalApi');
const Character = require('../../models/Character');

class CharactersController {
  constructor(model) {
    this._characterModel = model;
    this._limit = 10;

    this.getByPage = this.getByPage.bind(this);
    this.getById = this.getById.bind(this);
    this.search = this.search.bind(this);
    this.getAll = this.getAll.bind(this);
  }

  /**
   * @method
   *
   * This function will return all the available characters.
   *
   * @returns {Array} It will return an array of object.
   */
  async getAll(req, res) {
    let characters = await this._characterModel
      .find({}, null, {
        limit: this._limit
      })
      .exec();

    if (!characters.length) {
      // NOTE: this might be called somewhere else, like a task maybe
      // for now, I will call it here, it works though.
      const charactersExtApi = new CharactersExternalApi(Character);
      characters = await charactersExtApi.init();
    }

    return res.status(characters.length ? 200 : 404).json(characters);
  }

  /**
   * @method
   *
   * This function will return just one character by the id
   *
   * @returns {Object} A single object with all the needed character data
   */
  async getById(req, res) {
    const { id } = req.params;

    const character = await this._characterModel.findOne({ _id: id }).exec();

    return res
      .status(Object.keys(character).length ? 200 : 404)
      .json(character);
  }

  /**
   * @method
   *
   * This is for the pagination. In the front this is gonna be called only whe the data
   * doesn't already exist in the front.
   *
   * @returns {Array} It will return an array of object of the characters limited by 10.
   */
  async getByPage(req, res) {
    const { page } = req.query;

    const characters = await this._characterModel
      .find({}, null, {
        skip: 10 * page,
        limit: this._limit
      })
      .exec();

    return res.status(characters.length ? 200 : 404).json(characters);
  }

  /**
   * @method search
   *
   * This function is for making a search based on the criteria sent from the front.
   * In this case it might be two possible criteria: by name or by house.
   *
   * @returns {Array} It will return an array with all the characters that match the criteria
   *                  otherwise just an empty array
   */
  async search(req, res) {
    const { criteria } = req.params;

    const characters = await this._characterModel
      .find(
        {
          $or: [
            {
              name: {
                $regex: new RegExp(criteria, 'i')
              }
            },
            {
              house: {
                $regex: new RegExp(criteria, 'i')
              }
            }
          ]
        },
        'id house name'
      )
      .exec();

    return res.status(characters.length ? 200 : 404).json(characters);
  }
}

const characters = new CharactersController(Character);

module.exports = characters;
