// Modules
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const path = require('path');
// Project
const routes = require('./routes');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(helmet());
app.use(cors());

routes(app);

mongoose.connect(`mongodb://0.0.0.0:27017/tactech`, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

// Use this when you generate the react build.
// It's not ideal to keep everything monolithic, but works for this challenge
if (process.env.NODE_ENV === 'production') {
  const buildPath = path.join(path.dirname(__dirname), 'client', 'build');

  app.use(express.static(buildPath));

  app.get('/*', (req, res) => {
    res.sendFile(path.join(buildPath, 'index.html'));
  });
}

const PORT = process.env.PORT || 3001;

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`);
});

module.exports = app;
