const mongoose = require('mongoose');

const characterSchema = new mongoose.Schema({
  titles: [
    {
      type: String
    }
  ],
  books: [
    {
      type: String
    }
  ],
  name: String,
  slug: String,
  gender: String,
  image: String,
  house: String,
  rank: Number
});

const Character = mongoose.model('Characters', characterSchema);

module.exports = Character;
