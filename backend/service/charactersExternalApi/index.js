const https = require('https');

class CharactersExternalApi {
  constructor(model) {
    this._characterModel = model;
  }

  async init() {
    const response = await this._charactersFromApi();

    const characters = await this._insertCharactersFromApi(response);
    return characters;
  }

  _charactersFromApi() {
    // We will get the external data from the api.got.show service
    return new Promise(resolve => {
      https
        .get('https://api.got.show/api/general/characters', res => {
          res.setEncoding('utf8');

          let body = '';

          res.on('data', data => {
            body += data;
          });

          res.on('end', () => {
            const parsedBody = JSON.parse(body);
            if (parsedBody.success) {
              resolve(parsedBody.book);
            }
          });
        })
        .on('error', err => {
          console.error(err);
          process.exit(1);
        });
    });
  }

  _insertCharactersFromApi(data) {
    const bulkCharacters = data.map(
      ({ titles, books, name, slug, gender, image, house, pagerank }) => ({
        titles,
        books,
        name,
        slug,
        gender,
        image,
        house,
        rank: pagerank.rank
      })
    );

    return this._characterModel.insertMany(bulkCharacters);
  }
}

module.exports = CharactersExternalApi;
