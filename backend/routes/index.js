// Modules
const { Router } = require('express');
// Project
const paths = require('./paths');
const characters = require('./characters');


function routes(app) {
    const router = Router();

    // API - v1 (api/v1)
    app.use(paths.api.root, characters(router));
}

module.exports = routes;
