const CharactersController = require('./../../controllers/Characters');
const paths = require('../paths');

function characters(router) {
  // api/v1/characters
  router.get(paths.api.characters.getAll, CharactersController.getAll);
  // api/v1/characters/page
  router.get(paths.api.characters.getByPage, CharactersController.getByPage);
  // api/v1/characters/:id
  router.get(paths.api.characters.getById, CharactersController.getById);
  // api/v1/characters/search/:criteria
  router.get(paths.api.characters.search, CharactersController.search);

  return router;
}

module.exports = characters;
