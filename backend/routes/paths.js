const paths = {
  api: {
    root: '/api/v1',
    characters: {
      getAll: '/characters',
      getByPage: '/characters/page',
      getById: '/characters/:id',
      search: '/characters/search/:criteria'
    }
  }
};

module.exports = paths;
