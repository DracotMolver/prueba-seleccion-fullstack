import React, { useEffect, useState, Fragment } from 'react';
import produce from 'immer';
import axios from 'axios';
// UI
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
// components
import CharactersList from '../../components/CharactersList';
import SearchInput from '../../components/Widget/SearchInput';

let _time = 0;

const ListCharacters = () => {
  const [state, setState] = useState({
    characters: { 0: [] },
    isLoading: true,
    page: 0
  });

  const [searchState, setSearchState] = useState({
    searchCharacter: '',
    items: []
  });

  useEffect(() => {
    axios.get('http://localhost:3001/api/v1/characters').then(res => {
      if (res.status === 200) {
        setState(
          produce(draft => {
            draft.characters[draft.page] = res.data;
            draft.isLoading = false;
          })
        );
      }
    });
  }, []);

  function onChangePageHandle(page) {
    // NOTE: We could me all of this with useReducer
    // but let keep it simple :)
    setState(
      produce(draft => {
        draft.isLoading = true;
      })
    );

    // Let's apply memoization to this.
    // If we already called this page, don't make the call to the api and load the already
    // loaded data
    if (!state.characters[page]) {
      axios
        .get('http://localhost:3001/api/v1/characters/page', {
          params: {
            page
          }
        })
        .then(res => {
          if (res.status === 200) {
            setState(
              produce(draft => {
                draft.characters[page] = res.data;
                draft.isLoading = false;
                draft.page = page;
              })
            );
          }
        });
    } else {
      setState(
        produce(draft => {
          draft.page = page;
          draft.isLoading = false;
        })
      );
    }
  }

  function onChangeSearchHandle(event) {
    event.persist();

    const { value } = event.currentTarget;

    setSearchState(
      produce(draft => {
        draft.searchCharacter = value;
      })
    );

    clearTimeout(_time);
    _time = setTimeout(() => {
      axios
        .get(`http://localhost:3001/api/v1/characters/search/${value}`)
        .then(res => {
          if (res.status === 200) {
            setSearchState(
              produce(draft => {
                draft.items = res.data;
              })
            );
          }
        });

      clearTimeout(_time);
    }, 1200);
  }

  return (
    <Fragment>
      <Container>
        <CssBaseline />
        <SearchInput
          onChangeSearchHandle={onChangeSearchHandle}
          searchCharacter={searchState.searchCharacter}
          placeholder="Search by name or house"
          items={searchState.items}
        />
        <Box my={8}>
          <CharactersList
            onChangePageHandle={onChangePageHandle}
            characters={state.characters}
            isLoading={state.isLoading}
            page={state.page}
          />
        </Box>
      </Container>
    </Fragment>
  );
};

export default ListCharacters;
