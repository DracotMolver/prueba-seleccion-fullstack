import React, { useEffect, useState, Fragment } from 'react';
import { useLocation } from 'react-router-dom';
import produce from 'immer';
import axios from 'axios';
// UI
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import CharacterProfile from '../../components/CharacterProfile';
// components

const ViewCharacter = () => {
  const [state, setState] = useState({
    character: {}
  });

  const location = useLocation();
  const query = new URLSearchParams(location.search);

  useEffect(() => {
    axios
      .get(`http://localhost:3001/api/v1/characters/${query.get('id')}`)
      .then(res => {
        if (res.status === 200) {
          setState(
            produce(draft => {
              draft.character = res.data;
            })
          );
        }
      });
  }, []);

  return (
    <Fragment>
      <Container>
        <CssBaseline />
        <Box my={8}>
          <CharacterProfile character={state.character} />
        </Box>
      </Container>
    </Fragment>
  );
};

export default ViewCharacter;
