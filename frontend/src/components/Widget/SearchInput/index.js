import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// UI
import { makeStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import InputBase from '@material-ui/core/InputBase';
import ListItem from '@material-ui/core/ListItem';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    position: 'relative',
    padding: '2px 4px',
    width: 500
  },
  input: {
    marginLeft: theme.spacing(1),
    width: 430
  },
  iconButton: {
    padding: 10
  },
  items: props => ({
    overflowX: 'auto',
    maxHeight: 350,
    position: 'absolute',
    width: '100%',
    display: props.items.length ? 'block' : 'none',
    cursor: 'pointer'
  })
}));

const SearchInput = ({
  onChangeSearchHandle,
  searchCharacter,
  placeholder,
  items
}) => {
  const classes = useStyles({ items });

  return (
    <Grid
      container
      alignItems="flex-start"
      className={classes.root}
      direction="row"
      justify="flex-end"
    >
      <Paper className={classes.paper}>
        <InputBase
          className={classes.input}
          placeholder={placeholder}
          onChange={onChangeSearchHandle}
          value={searchCharacter}
        />
        <IconButton
          type="submit"
          className={classes.iconButton}
          aria-label="search"
          label="Search"
        >
          <SearchIcon />
        </IconButton>
        <Box className={classes.items} boxShadow={1}>
          <Paper elevation={3}>
            <List component="nav" aria-label="List of characters">
              {items.map(({ _id, name, house }) => (
                <Link
                  key={_id}
                  to={{
                    pathname: '/character',
                    search: `?id=${_id}`
                  }}
                >
                  <ListItem divider>
                    <ListItemText primary={name} secondary={house} />
                  </ListItem>
                </Link>
              ))}
            </List>
          </Paper>
        </Box>
      </Paper>
    </Grid>
  );
};

SearchInput.defaultProps = {
  searchCharacter: '',
  items: []
};

SearchInput.propTypes = {
  onChangeSearchHandle: PropTypes.func,
  searchCharacter: PropTypes.string,
  placeholder: PropTypes.string,
  items: PropTypes.array
};

export default SearchInput;
