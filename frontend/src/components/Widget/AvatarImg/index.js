import React from 'react';
import PropTypes from 'prop-types';
// ui
import CardMedia from '@material-ui/core/CardMedia';

const AvatarImg = ({ className, title, image }) =>
  image ? (
    <CardMedia className={className} image={image} title={title} />
  ) : null;

AvatarImg.defaultProps = {
  image: '',
  title: ''
};

AvatarImg.propTypes = {
  className: PropTypes.string,
  image: PropTypes.string,
  title: PropTypes.string
};

export default AvatarImg;
