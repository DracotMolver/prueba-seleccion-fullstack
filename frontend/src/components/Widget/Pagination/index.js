import React from 'react';
import PropTypes from 'prop-types';
// ui
import { makeStyles } from '@material-ui/core/styles';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import IconButton from '@material-ui/core/IconButton';

const useStyles = makeStyles(theme => ({
  root: {
    marginLeft: theme.spacing(2.5)
  }
}));

const Pagination = props => {
  const classes = useStyles();

  function onClickNextHandle(event) {
    const { onChangePageHandle, page } = props;

    onChangePageHandle(page + 1);
  }

  function onClickBackHandle(event) {
    const { onChangePageHandle, page } = props;

    onChangePageHandle(page - 1);
  }

  const { page } = props;

  return (
    <div className={classes.root}>
      Page: {page + 1}
      <IconButton
        onClick={onClickBackHandle}
        aria-label="previous page"
        disabled={page === 0}
      >
        <KeyboardArrowLeft />
      </IconButton>
      <IconButton onClick={onClickNextHandle} aria-label="next page">
        <KeyboardArrowRight />
      </IconButton>
    </div>
  );
};

Pagination.defaultProps = {
  page: 0
};

Pagination.propTypes = {
  onChangePageHandle: PropTypes.func,
  page: PropTypes.number
};

export default Pagination;
