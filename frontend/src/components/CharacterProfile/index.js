import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// ui
import { makeStyles } from '@material-ui/core/styles';
import ArrowBack from '@material-ui/icons/ArrowBack';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';

// component
import Description from './Description';
import AvatarImg from '../Widget/AvatarImg';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  media: {
    height: 0,
    paddingTop: '100%'
  },
  button: {
    marginTop: theme.spacing(1)
  }
}));

const CharacterProfile = ({ character }) => {
  const classes = useStyles();

  const { gender, house, name, image, titles, books, slug, rank } = character;

  return (
    <Fragment>
      <Paper>
        <Grid container className={classes.root} spacing={2}>
          <Grid item xs={4}>
            <AvatarImg className={classes.media} image={image} title={name} />
          </Grid>
          <Grid item xs={6}>
            <Description
              titles={titles}
              gender={gender}
              books={books}
              house={house}
              name={name}
              slug={slug}
              rank={rank}
            />
          </Grid>
        </Grid>
      </Paper>
      <Grid container className={classes.root}>
        <Grid item xs={2}>
          <Link to="/">
            <Button
              variant="contained"
              color="secondary"
              className={classes.button}
              startIcon={<ArrowBack />}
            >
              Back to the list
            </Button>
          </Link>
        </Grid>
      </Grid>
    </Fragment>
  );
};

CharacterProfile.defaultProps = {
  character: {}
};

CharacterProfile.propTypes = {
  character: PropTypes.object,
};

export default CharacterProfile;
