import React from 'react';
import PropTypes from 'prop-types';
// ui
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';

const Description = ({ rank, books, slug, titles, name, house }) => (
  <List component="nav" aria-label="main mailbox folders">
    <ListItemText primary="Books" secondary={books.join(' / ')} />
    <ListItemText primary="Name" secondary={name} />
    <ListItemText primary="House" secondary={house} />
    <ListItemText primary="Titles" secondary={titles.join(' / ')} />
    <ListItemText primary="Slug" secondary={slug} />
    <ListItemText primary="Rank" secondary={rank} />
  </List>
);

Description.defaultProps = {
  titles: [],
  books: [],
  slug: '',
  name: '',
  rank: 0,
  house: ''
};

Description.propTypes = {
  titles: PropTypes.array,
  books: PropTypes.array,
  slug: PropTypes.string,
  name: PropTypes.string,
  rank: PropTypes.number,
  house: PropTypes.string
};

export default Description;
