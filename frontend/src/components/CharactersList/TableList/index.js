import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
// ui
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const TableList = ({ name, house, rank, id }) => (
  <TableRow>
    <TableCell component="th" scope="row">
      {name}
    </TableCell>
    <TableCell align="right">{house}</TableCell>
    <TableCell align="right">{rank}</TableCell>
    <TableCell align="right">
      <Link
        to={{
          pathname: '/character',
          search: `?id=${id}`
        }}
      >
        Visit Profile
      </Link>
    </TableCell>
  </TableRow>
);

TableList.defaultProps = {
  name: 'Unknown',
  house: 'Unknown',
  rank: 'Unknown'
};

TableList.propTypes = {
  name: PropTypes.string,
  house: PropTypes.string,
  rank: PropTypes.number,
  id: PropTypes.string
};

export default TableList;
