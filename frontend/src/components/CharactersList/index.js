import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// Styles
import LinearProgress from '@material-ui/core/LinearProgress';
import { makeStyles } from '@material-ui/core/styles';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Table from '@material-ui/core/Table';
import Paper from '@material-ui/core/Paper';
// Components
import Pagination from '../Widget/Pagination';
import TableList from './TableList';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%'
  },
  linearProgress: props => ({
    padding: theme.spacing(1, 0),
    marginTop: theme.spacing(2),
    display: props.isLoading ? 'block' : 'none'
  })
}));

const CharactersList = ({
  onChangePageHandle,
  characters,
  isLoading,
  page
}) => {
  const classes = useStyles({ isLoading });

  return (
    <Fragment>
      <TableContainer className={classes.root} component={Paper}>
        <LinearProgress className={classes.linearProgress} color="secondary" />
        <Table aria-label="GOT list characters">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell align="right">House</TableCell>
              <TableCell align="right">Rank</TableCell>
              <TableCell align="right">Profile</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {characters[page].map(character => (
              <TableList
                key={character._id}
                house={character.house}
                name={character.name}
                rank={character.rank}
                id={character._id}
              />
            ))}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell
                padding="none"
                variant="footer"
                colSpan={4}
                align="right"
              >
                <Pagination
                  onChangePageHandle={onChangePageHandle}
                  page={page}
                />
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </Fragment>
  );
};

CharactersList.defaultProps = {
  isLoading: false,
  page: 0
};

CharactersList.propTypes = {
  onChangePageHandle: PropTypes.func,
  characters: PropTypes.objectOf(PropTypes.array),
  isLoading: PropTypes.bool,
  page: PropTypes.number
};

export default CharactersList;
