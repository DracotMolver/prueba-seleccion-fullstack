import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
// Containers
import ListCharacters from '../containers/ListCharacters';
import ViewCharacter from '../containers/ViewCharacter';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={ListCharacters} />
      <Route exact path="/character" component={ViewCharacter} />
    </Switch>
  </BrowserRouter>
);

export default App;
